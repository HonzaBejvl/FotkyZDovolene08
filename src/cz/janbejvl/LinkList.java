package cz.janbejvl;



public class LinkList {
    Link first, current;

    void insert(char letter) {
        Link newLink = new Link();
        newLink.data = letter;
        if (current == null) {
           // newLink.next = first;
            first = newLink;
            current = first;
        }
        else {
            newLink.next = current.next;
            current.next = newLink;
            //newLink.next = current;
            //if (current == first)
            //    first = newLink;
            //current = newLink;
        }
    }

    void next() throws Exception {
        if (first == null)
            throw new Exception();
        if (current == null) {
            current = first;
        }
        else
        {
            current = current.next;
            if (current == null)
                throw new Exception();
        }
    }

    void remove() throws Exception {
        if (current == null)
            throw new Exception();
        else {
            first = first.next;
        }
    }

    char get() throws Exception {

        if (first == null) {
            throw new Exception();
        }
        if (current == null) {
            return first.data;
        }
        if (current.next!=null) {
            return current.next.data;
        }
        else throw new Exception();
    }

    void moveToFirst() {
        current = null;
    }

    boolean hasNext() {
        if (current == null) {
            if (first != null)
                return true;
            else return false;
        }
        if (current.next!=null)
            return true;
        else return false;
    }
}
