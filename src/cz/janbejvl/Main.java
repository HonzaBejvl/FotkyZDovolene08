package cz.janbejvl;

public class Main {

    public static void main(String[] args) throws Exception {
        LinkList myList = new LinkList();
        myList.insert('a');
        myList.insert('k');
        myList.insert('l');
        myList.next();
        myList.insert('a');
        myList.next();
        myList.insert('s');

        printList(myList);

        myList.moveToFirst();
        myList.insert('v');
        myList.insert('i');
        myList.insert('p');
        myList.next();
        myList.next();
        myList.next();
        myList.remove();
        myList.remove();
        myList.remove();
        myList.remove();
        myList.insert('o');
        myList.next();
        myList.remove();

        printList(myList);    }

    public static void printList(LinkList list)
    {
        Link item = list.first;
        String ret = "" + item.data;
        while (item.next != null)
        {
            item = item.next;
            ret += item.data;
        }
        System.out.println(ret);
    }
}
